<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'PlateController@start');
Route::get('/go', 'PlateController@go');
Route::get('/home', 'PlateController@home');
Route::get('/restart', 'PlateController@restart');


Route::get('/recipe/detail/{id}', 'RecipeController@detail');
Route::get('/plate/status/{id}', 'PlateController@status');

Route::get('/purchases', 'OrderController@index');
Route::get('/recipes', 'RecipeController@index');
Route::get('/history', 'PlateController@index');

Route::get('/restart_database', 'PlateController@restart_database');


