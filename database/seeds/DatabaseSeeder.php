<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		DB::table('ingredients')->insert([
			['name' => 'tomato', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'lemon', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'potato', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'rice', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'ketchup', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'lettuce', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'onion', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'cheese', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'meat', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'chicken', 'available' => 5, 'created_at' => date('Y-m-d H:i:s')]]);
		DB::table('recipes')->insert([
			['name' => 'Recipe 1', 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'Recipe 2', 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'Recipe 3', 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'Recipe 4', 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'Recipe 5', 'created_at' => date('Y-m-d H:i:s')],
			['name' => 'Recipe 6', 'created_at' => date('Y-m-d H:i:s')]]);		
		$ran = array(1,2,3,4,5,6,7,8,9,10);
		for ($i=1; $i <= 6; $i++) { 
			$ingredients_amount = rand(2, 3);
			for($j=1; $j<= $ingredients_amount; $j++)
			{
				$key = rand(0, count($ran)-1);
				$ingredient_id = $ran[$key];
				$quantity = rand(1, 3);
				DB::table('recipe_ingredients')->insert([
					'recipe_id' => $i,
					'ingredient_id' => $ingredient_id,
					'quantity' => $quantity]);
				array_splice($ran, $key, 1);
				if(count($ran) == 0)
				{
				$ran = array(1,2,3,4,5,6,7,8,9,10);	
				}
			}

		}
    }
}
