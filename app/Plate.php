<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plate extends Model
{
    protected $table = 'plates';
    protected $fillable = ['recipe_id', 'status', 'created_at', 'updated_at'];
    protected $guarded = ['id'];    

    public function recipe()
    {
        return $this->hasOne('App\Recipe', 'id', 'recipe_id');
    }

}
