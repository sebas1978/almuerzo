<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    //
    protected $table = 'recipes';
    protected $fillable = ['name', 'created_at', 'updated_at'];
    protected $guarded = ['id'];    

    public function ingredients(){
    	return $this->belongsToMany('\App\Ingredient','recipe_ingredients')
     				->withPivot('recipe_id', 'quantity');
     				}     
}
