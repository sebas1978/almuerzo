<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    //
    protected $table = 'ingredients';
    protected $fillable = ['name', 'available', 'created_at', 'updated_at'];
    protected $guarded = ['id'];    

    public function recipes(){
    	return $this->belongsToMany('\App\Recipe','recipe_ingredients')
     				->withPivot('ingredient_id');
     				}    
}
