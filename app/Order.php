<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';
    protected $fillable = ['ingredient_id', 'quantity', 'plate_id', 'created_at', 'updated_at'];
    protected $guarded = ['id'];    

    public function ingredient()
    {
        return $this->hasOne('App\Ingredient', 'ingredient_id');
    }    
    public function plate()
    {
        return $this->hasOne('App\Plate', 'plate_id');
    }        
}
