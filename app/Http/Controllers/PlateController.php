<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;
use Response;



use App\Jobs\PreparePlate;
use App\Plate;
use App\Recipe;
use App\Ingredient;
use App\Order;

use DB;
use Carbon;

class PlateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function start(){
        return view('start');
    }

    public function go(){
        $exist = DB::table('information_schema.tables')
                    ->where('table_schema', 'jornada')
                    ->where('table_name', 'migrations')
                    ->first();
        if($exist)
            {
            Artisan::call('migrate:rollback', ['--force'=> true]);            
            }            
        Artisan::call('migrate', ['--force'=> true]);        
        Artisan::call('db:seed');        
        return view('index');
    }

    public function home(){
        return view('index');
    }

    public function restart(){
        $pending = DB::table('plates')
                    ->where('plates.status', '<', 3)
                    ->select('*')
                    ->orderBy('plates.created_at', 'asc')
                    ->get();        
        $pending->each(function($item) 
                    {
                        PreparePlate::dispatch($item->id);  
                    });
    }
    public function getplates()
    {

    $pending = DB::table('plates')
                ->join('recipes', 'plates.recipe_id', '=', 'recipes.id')
                ->join('recipe_ingredients', 'recipes.id', '=', 'recipe_ingredients.recipe_id')
                ->join('ingredients', 'recipe_ingredients.ingredient_id', '=', 'ingredients.id')
                ->where('plates.status', '<', 3)
                ->select('plates.*', 'recipes.name as recipe_name', DB::raw("group_concat(recipe_ingredients.quantity,' ',ingredients.name SEPARATOR ', ') as ingredients"))
                ->orderBy('plates.updated_at', 'desc')
                ->orderBy('ingredients.name', 'asc')
                ->groupBy('plates.id')   
                ->get();
    $pending->each(function($item)
        {
         $item->ago = Carbon\Carbon::parse($item->updated_at)->diffForHumans();
        });                

    $complete = DB::table('plates')
                ->join('recipes', 'plates.recipe_id', '=', 'recipes.id')
                ->join('recipe_ingredients', 'recipes.id', '=', 'recipe_ingredients.recipe_id')
                ->join('ingredients', 'recipe_ingredients.ingredient_id', '=', 'ingredients.id')
                ->where('plates.status', '=', 3)
                ->select('plates.*', 'recipes.name as recipe_name', DB::raw("group_concat(recipe_ingredients.quantity,' ',ingredients.name SEPARATOR ', ') as ingredients"))
                ->orderBy('plates.updated_at', 'desc')
                ->orderBy('ingredients.name', 'asc')
                ->groupBy('plates.id')   
                ->take(10)
                ->get();
    $complete->each(function($item)
        {
         $item->ago = Carbon\Carbon::parse($item->updated_at)->diffForHumans();
        });      

    $warehouse = DB::table('ingredients')
                    ->get();

    return Response::json(array('pending'=>$pending, 'complete'=>$complete, 'warehouse'=>$warehouse));  
       
    }

 
    public function index()
    {
        $plates = Plate::orderBy('updated_at', 'desc')->get();
        //dd($plates);
        return view('history')
            ->with('plates',$plates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        set_time_limit(0);
        if($request->amount > 0)
        {
            $ids = array();
            for($i=0;$i<$request->amount;$i++)
            {
                $plate = new Plate;
                $plate->recipe_id = rand(1, 6);
                $plate->status = 0;
                $plate->save();   
                $ids[] = $plate->id;
            }
            for($i=0;$i<count($ids);$i++)
            {
            PreparePlate::dispatch($ids[$i]);    
            }
            
        }
    }

    public function restart_database()
    {
    Artisan::call('migrate:refresh', ['--force'=> true]);        
    Artisan::call('db:seed');
    dd('done ;)');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
