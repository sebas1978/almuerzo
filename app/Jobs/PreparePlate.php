<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Plate;
use App\Recipe;
use App\Ingredient;
use App\Order;

class PreparePlate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        set_time_limit(0);

        $plate = Plate::find($this->id);
        $recipe = Recipe::find($plate->recipe_id);
        Plate::find($this->id)->update(['status' => 1]);
        foreach ($recipe->ingredients as $ingredient) {
            if($ingredient->available >= $ingredient->pivot->quantity)//hay stock
                {
                 $minus = $ingredient->available - $ingredient->pivot->quantity;
                 Ingredient::find($ingredient->id)->update(['available' => $minus]);   
                }
                else //hay que comprar
                {
                 Plate::find($this->id)->update(['status' => 2]);   
                 $add = 0;
                 while(($ingredient->available + $add) < $ingredient->pivot->quantity)
                    {
                     $ch = curl_init();
                     curl_setopt($ch, CURLOPT_URL, "https://recruitment.alegra.com/api/farmers-market/buy?ingredient=".$ingredient->name);
                     curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);      
                     $resultado = json_decode(curl_exec($ch));               
                     $add = $resultado->quantitySold;
                     $order = new Order;
                     $order->ingredient_id = $ingredient->id;
                     $order->quantity = $add;
                     $order->plate_id = $this->id;
                     $order->save();
                     $more = $ingredient->available + $add;
                     Ingredient::find($ingredient->id)->update(['available' => $more]);                     
                     $minus = $more - $ingredient->pivot->quantity;
                     Ingredient::find($ingredient->id)->update(['available' => $minus]);   
                    }    
                }    
        }
        Plate::find($this->id)->update(['status' => 1]);
        sleep(rand(5, 15));
        Plate::find($this->id)->update(['status' => 3]);        
    }
}
