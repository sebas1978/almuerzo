var app = angular.module('app', [], function($interpolateProvider) 
    {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    });

app.constant('Server', server + '/api/');
app.constant('Front', server);

app.controller("appController", ['$scope', '$rootScope', '$timeout', '$interval', '$http', 'Server', 'Front',  function($scope, $rootScope, $timeout, $interval, $http, Server, Front) {
$rootScope.base = Server;
$rootScope.front = Front;

$scope.preparing = [];
$scope.completed = [];
$scope.warehouse = [];
$scope.cantidad = 1;
$scope.received = false;

var config = {
  async:true
};

$http.get($rootScope.front+'restart', config).then(function successCallback(response) {
    console.log('reinicie ordenes');
}, function errorCallback(response) {
    console.log('error reiniciando o no hay platos pendientes');
});

$scope.solicitar = function()
{

$scope.received = true;
var formData = new FormData();
  formData.append('amount', $scope.cantidad);
  $http.post($rootScope.base+'plate/create', formData, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined, 'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')}
      }).then(function successCallback(response) {
      	//ok
    }, function errorCallback(response) {
      //ko
    });

$timeout(function() {
		      $scope.received = false;
		    }, 5000);      
$scope.cantidad = 1;


}

var timer=$interval(function()
	{
		$http({method: 'GET', url: 'api/getplates'}).then(function(data) {
			$scope.preparing = data.data.pending;
			$scope.completed = data.data.complete;
			$scope.warehouse = data.data.warehouse;
		});	
    },1500); //1500 =  un segundo y medio

}]);