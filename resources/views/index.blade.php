<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <meta charset="utf-8">
    <base href="{{  $_ENV['APP_URL'] }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Sebastian de alegra.com">
    <title>Jornada de almuerzo ¡gratis!</title>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <!-- Bootstrap core CSS -->
	  <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="">
		<script>
			var server = "{{ request()->getSchemeAndHttpHost() }}";
		</script>		
		
  </head>
  <body >
    <header>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container d-flex justify-content-between">
      <a href="home" class="navbar-brand d-flex align-items-center">
        <i class="fas fa-home" style="margin-right: 10px"></i>
        <strong>Home</strong>
      </a>
      <a href="purchases" class="navbar-brand d-flex align-items-center">
        <i class="fas fa-box" style="margin-right: 10px"></i>
        <strong>Purchases</strong>
      </a>
      <a href="recipes" class="navbar-brand d-flex align-items-center">
        <i class="far fa-list-alt" style="margin-right: 10px"></i>
        <strong>Recipes</strong>
      </a>
      <a href="history" class="navbar-brand d-flex align-items-center">
        <i class="far fa-list-alt" style="margin-right: 10px"></i>
        <strong>History</strong>
      </a>
    </div>
  </div>
</header>

<main role="main" ng-controller="appController">

  <div class="album py-5 bg-light">
    <div class="container">
	  <div class="row">
      	<div class="col-md-3">
	        <div class="card mb-4 shadow-sm">
		        <div class="card-body">
		            <p class="card-title">Get Plates</p>
		            <hr>
			      	<input type="number" name="input" ng-model="cantidad" min="0" max="99" required>
			        <a ng-click="solicitar()" class="btn btn-primary my-2">Get Plate's</a>
			        <p class="card-text" ng-show="received" style="color: #FF0000FF" ng-cloak>Order received</p>
	        	</div>
	         </div>
    	</div>
      	<div class="col-md-9" ng-cloak>
	        <div class="card mb-3 shadow-sm" style="background-color: #FF0000FF">
		        <div class="card-body">
		            <p class="card-title" style="color: #FFF">Warehouse</p>
		            <hr>
		            <div class="row">
		            	<div class="col-md-3" ng-repeat="item in warehouse" ng-cloak>
		            		<div class="card mb-4 shadow-sm"  style="background-color: #FFF; margin-bottom: 5px;">
											<div class="card-body">
												<p class="card-text">[[item.name]]: [[item.available]]</p>
											</div>	
										</div>
									</div>
								</div>
	        	</div>
	         </div>
    	</div>    	
	  </div>		
      <div class="row">
      	<div class="col-md-6">
      	  <p class="lead text-muted" style="text-align: center;">Pending</p>	
      	  	<div class="row">
		        <div class="col-md-6" ng-repeat="item in preparing" ng-cloak>
		          	<div class="card mb-4 shadow-sm" style="background-color: #FFD800FF">
			            <div class="card-body">
			              <p class="card-title">[[item.recipe_name]]</p>
			              <hr>
			              <p class="card-text">[[item.ingredients]]</p>
			              <p class="card-text" ng-show="[[item.status == 0]]">Pending</p>
			              <p class="card-text" ng-show="[[item.status == 1]]">Preparing</p>
			              <p class="card-text" ng-show="[[item.status == 2]]">Buying</p>
			              <div class="d-flex justify-content-between align-items-center">
			                <small class="text-muted">[[item.ago]]</small>
			              </div>
			            </div>
		          	</div>
		        </div>
		    </div>   
    	</div>
      	<div class="col-md-6">
      	  <p class="lead text-muted" style="text-align: center;">Latest 10 Completed</p>	
      	  	<div class="row">
		        <div class="col-md-6" ng-repeat="item in completed" ng-cloak>
		          	<div class="card mb-4 shadow-sm" style="background-color:  #B6FF00FF">
			            <div class="card-body">
			              <p class="card-title">[[item.recipe_name]]</p>
			              <hr>
			              <p class="card-text">[[item.ingredients]]</p>
			              <div class="d-flex justify-content-between align-items-center">
			                <small class="text-muted">[[item.ago]]</small>
			              </div>
			            </div>
		          	</div>
		        </div>
	        </div>	
    	</div>    	
    </div>
  </div>

</main>

<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://getbootstrap.com/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>    
</body>
</html>
