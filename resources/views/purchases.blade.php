<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <meta charset="utf-8">
    <base href="{{  $_ENV['APP_URL'] }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Sebastian de alegra.com">
    <title>Jornada de almuerzo ¡gratis!</title>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <!-- Bootstrap core CSS -->
  <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="">
  </head>
  <body >
    <header>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container d-flex justify-content-between">
      <a href="home" class="navbar-brand d-flex align-items-center">
        <i class="fas fa-home" style="margin-right: 10px"></i>
        <strong>Home</strong>
      </a>
      <a href="purchases" class="navbar-brand d-flex align-items-center">
        <i class="fas fa-box" style="margin-right: 10px"></i>
        <strong>Purchases</strong>
      </a>
      <a href="recipes" class="navbar-brand d-flex align-items-center">
        <i class="far fa-list-alt" style="margin-right: 10px"></i>
        <strong>Recipes</strong>
      </a>
      <a href="history" class="navbar-brand d-flex align-items-center">
        <i class="far fa-list-alt" style="margin-right: 10px"></i>
        <strong>History</strong>
      </a>
    </div>
  </div>
</header>

<main role="main">
  	<div class="album py-5 bg-light">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-md-12">
					<h2 class="mt-4">Purchases</h2>
					<div class="row mb-3">
					    <div class="col-md-6 themed-grid-col">Ingredient</div>
					    <div class="col-md-2 themed-grid-col">Quantity</div>
					    <div class="col-md-4 themed-grid-col">Date</div>
					 </div>
					 @foreach($orders as $item)
					 <div class="row mb-3">
					    <div class="col-md-6 themed-grid-col">{{ $item->name  }}</div>
					    <div class="col-md-2 themed-grid-col">{{ $item->quantity }}</div>
					    <div class="col-md-4 themed-grid-col">{{ date("d-m-Y H:i", strtotime($item->created_at)) }}</div>					 
					 </div>   
					 @endforeach
				</div>  
			</div>	
		</div>	
	</div>				  

</main>

<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://getbootstrap.com/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

</body>
</html>
